import {Component, Input, OnInit, Output, EventEmitter} from '@angular/core';
import { Task } from '../model/task';
import {TaskService} from '../services/task.service';

@Component({
  selector: 'app-done-task',
  templateUrl: './done-task.component.html',
  styleUrls: ['./done-task.component.css']
})
export class DoneTaskComponent implements OnInit {

  private doneTasksList: Array<Task> = [];

  constructor(private taskService: TaskService) {
    this.taskService.getDoneTasksList().subscribe((tasks: Array<Task>) => {
      this.doneTasksList = tasks;
    });
  }

  getDoneTasksList(): Array<Task>
  {
    return this.doneTasksList;
  }

  removeDoneTask(index: number)
  {
    this.taskService.removeCompletedTask(index);
  }

  ngOnInit(): void {
  }

}
