import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {TaskService} from '../services/task.service';
import {Task} from '../model/task';

@Component({
  selector: 'app-add-task',
  templateUrl: './add-task.component.html',
  styleUrls: ['./add-task.component.css']
})
export class AddTaskComponent implements OnInit {

  newTask: string = '';

  constructor(private tasksService: TaskService) {
  }


  addTask(): void
  {
    let  min = Math.ceil(1);
    let max = Math.floor(100000000);

    const task: Task = ({
      id: Math.floor(Math.random() * (max - min)) + min,
      name: this.newTask,
      created: new Date()
    });
    console.log(task);
    this.tasksService.addTask(task);
    this.newTask = '';
  }

  ngOnInit(): void {
  }

}
