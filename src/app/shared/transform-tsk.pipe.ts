import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'transformTask'
})
export class TransformTskPipe implements PipeTransform {

  transform(value: string, charAtEnd?: string): unknown {
    if (!value) return value;
    if (!charAtEnd) charAtEnd = '';
    return value[0].toUpperCase() + value.substr(1).toLowerCase() + charAtEnd;
  }

}
