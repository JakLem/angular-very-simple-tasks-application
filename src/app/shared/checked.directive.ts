import {Directive, ElementRef, OnInit, Renderer2} from '@angular/core';

@Directive({
  selector: '[lemSetRedColor]'
})
export class CheckedDirective implements OnInit{

  constructor(private elRef: ElementRef, private renderer: Renderer2) {

  }

  ngOnInit(): void {
    this.renderer.setStyle(this.elRef.nativeElement, "color", "red");
  }

}
