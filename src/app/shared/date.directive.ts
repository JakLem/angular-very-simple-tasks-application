import {Directive, ElementRef, HostListener, Input, Renderer2} from '@angular/core';

@Directive({
  selector: '[showDate]'
})
export class DateDirective {

  @Input()
  public date: Date | undefined;
  private paragraph;

  constructor(private elRef: ElementRef, private renderer: Renderer2) {
    this.paragraph = this.renderer.createElement('p');
  }

  @HostListener('mouseenter')
  mouseEnter(eventDate: Event)
  {
    this.paragraph.innerHTML = this.date?.toLocaleString();
    this.renderer.appendChild(this.elRef.nativeElement, this.paragraph);
  }

  @HostListener('mouseleave')
  mouseLeave(eventDate: Event)
  {
    this.paragraph.innerHTML = "";
    this.renderer.removeChild(this.elRef.nativeElement, this.paragraph);
  }


}
