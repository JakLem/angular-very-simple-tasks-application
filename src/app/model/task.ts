export interface Task {
  id: number,
  name: string;
  created: Date;
  end?: Date;
}
