import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import { Task } from '../model/task';
import {TaskService} from '../services/task.service';

@Component({
  selector: 'app-todo-task',
  templateUrl: './todo-task.component.html',
  styleUrls: ['./todo-task.component.css']
})
export class TodoTaskComponent implements OnInit {

  public tasksList: Array<Task> = [];

  constructor(private tasksService: TaskService) {
    this.tasksService.getTasksList().subscribe((tasks: Array<Task>) => {
      this.tasksList = tasks;
    });
  }

  completeTask(index: number): void
  {
    this.tasksService.completeTask(index);
  }

  removeTask(index: number)
  {
    this.tasksService.removeTask(index);
  }

  getTasksList(): Array<Task>
  {
    return this.tasksList;
  }


  ngOnInit(): void {
  }

}
