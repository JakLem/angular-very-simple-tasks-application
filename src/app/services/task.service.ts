import {Injectable, OnChanges, SimpleChanges} from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';
import { Task } from '../model/task';

@Injectable()
export class TaskService  {

  private tasksList: Array<Task> = [];
  private tasksListObs = new BehaviorSubject<Array<Task>>(this.tasksList);

  private doneTasksList: Array<Task> = [];
  private doneTasksListObs = new BehaviorSubject<Array<Task>>(this.doneTasksList);

  refreshTasksList()
  {
    this.tasksListObs.next(this.tasksList);
  }

  refreshDoneList()
  {
    this.doneTasksListObs.next(this.doneTasksList);
  }

  addTask(task: Task): void
  {
    this.tasksList.push(task);
    this.refreshTasksList();
  }

  removeTask(index: number): void
  {
    this.tasksList = this.tasksList.filter(t => t.id !== index);
    this.refreshTasksList();
  }

  completeTask(index: number): void
  {
    let doneTask: Array<Task> = this.tasksList.filter(t => t.id === index);
    doneTask[0].end = new Date(); //todo to refactor
    this.doneTasksList = this.doneTasksList.concat(doneTask); // merging two arrays

    this.refreshDoneList();
    this.removeTask(index);
  }

  removeCompletedTask(index: number): void
  {
    this.doneTasksList = this.doneTasksList.filter(t => t.id !== index);
    this.refreshDoneList();
  }

  getTasksList(): Observable<Array<Task>>
  {
    return this.tasksListObs.asObservable();
  }

  getDoneTasksList(): Observable<Array<Task>>
  {
    return this.doneTasksListObs.asObservable();
  }
}
